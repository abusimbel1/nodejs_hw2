const { Router } = require("express")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const { check, validationResult } = require("express-validator")
const config = require("config")
const User = require("../models/User")
const router = Router()

// /api/auth/register
router.post(
    "/register",
    [
        check("username", "Minimum length 3 characters").isLength({ min: 3 }),
        check("password", "Minimum length 4 characters").isLength({ min: 4 }),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect registration data",
                })
            }

            const { username, password } = req.body

            const candidate = await User.findOne({ username })

            if (candidate) {
                return res
                    .status(400)
                    .json({ message: "This user already exist" })
            }

            const hashedPass = await bcrypt.hash(password, 10)
            const user = new User({ username, password: hashedPass })

            await user.save()

            res.status(201).json({ message: "User created" })
        } catch (e) {
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

// /api/auth/login
router.post(
    "/login",
    [
        check("username", "Enter correct username").isLength({ min: 3 }),
        check("password", "Enter password").exists(),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect login data",
                })
            }

            const { username, password } = req.body
            const user = await User.findOne({ username })

            if (!user) {
                return res
                    .status(400)
                    .json({ message: "Incorrect username or password" })
            }

            const isMathPass = await bcrypt.compare(password, user.password)

            if (!isMathPass) {
                return res
                    .status(400)
                    .json({ message: "Incorrect username or password" })
            }

            const token = jwt.sign(
                JSON.stringify(user),
                config.get("jwtSectet")
            )

            res.json({ jwt_token: token })
        } catch (e) {
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

module.exports = router
