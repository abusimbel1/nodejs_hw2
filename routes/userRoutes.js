const { Router } = require("express")
const router = Router()
const auth = require("../middleware/authMiddleware")
const bcrypt = require("bcryptjs")
const User = require("../models/User")
const Note = require("../models/Note")

// api/users/me
router.get("/me", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        res.json({
            user: {
                _id: user._id,
                username: user.username,
                createdDate: user.createdDate,
            },
        })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/users/me
router.delete("/me", auth, async (req, res) => {
    try {
        const deletedUser = await User.findByIdAndDelete(req.user._id)
        if (!deletedUser) {
            return res.status(400).json({ message: "User not found" })
        }
        await Note.deleteMany({ userId: deletedUser._id })
        res.json({ message: "Success" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
// api/users/me
router.patch("/me", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const { oldPassword, newPassword } = req.body
        if (!oldPassword || !newPassword) {
            return res.status(400).json({ message: "No input params" })
        }
        const isMathPass = await bcrypt.compare(
            req.body.oldPassword,
            user.password
        )
        if (!isMathPass) {
            return res.status(400).json({ message: "Invalid old password" })
        }
        const hashedPass = await bcrypt.hash(newPassword, 10)
        await User.findByIdAndUpdate(req.user._id, { password: hashedPass })
        res.json({ message: "Success" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

module.exports = router
