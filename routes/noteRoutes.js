const { Router } = require("express")
const router = Router()
const auth = require("../middleware/authMiddleware")
const Note = require("../models/Note")
const User = require("../models/User")

// api/notes/
router.get("/", auth, async (req, res) => {
    //TODO test again with arr of notes
    try {
        const user = await User.findById(req.user._id)
        const notes = await Note.find({ _id: user.notes })
        res.json({ notes })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/notes/:id
router.get("/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user.notes.includes(req.params.id)) {
            return res.status(400).json({ message: "No note found" })
        }
        const note = await Note.findById(req.params.id)
        res.json({ note })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/notes/
router.post("/", auth, async (req, res) => {
    try {
        const { text } = req.body
        if (!text || text.trim().length < 1) {
            return res.status(400).json({ message: `Field 'text' not found` })
        }
        const note = new Note({ userId: req.user._id, text })
        const user = await User.findById(req.user._id)
        await User.findByIdAndUpdate(req.user._id, {
            notes: [...user.notes, note._id],
        })
        await note.save()
        res.status(201).json({ message: "Note created" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/notes/:id
router.delete("/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user.notes.includes(req.params.id)) {
            return res.status(400).json({ message: "No note found" })
        }

        const filtredUserNotesArr = user.notes.filter(
            (item) => item != req.params.id
        )
        console.log(filtredUserNotesArr)
        await Note.findByIdAndDelete(req.params.id)
        await User.findByIdAndUpdate(req.user._id, {
            notes: filtredUserNotesArr,
        })
        res.status(200).json({ message: "Note deleted" })
    } catch (e) {
        console.log(e.message)
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/notes/:id
router.put("/:id", auth, async (req, res) => {
    try {
        const { text } = req.body
        if (!text || text.trim().length < 1) {
            return res.status(400).json({ message: `Field 'text' not found` })
        }
        const user = await User.findById(req.user._id)
        if (!user.notes.includes(req.params.id)) {
            return res.status(400).json({ message: "No note found" })
        }

        await Note.findByIdAndUpdate(req.params.id, { text })
        res.status(200).json({ message: "Note updated" })
    } catch (e) {
        console.log(e.message)
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

//api/notes/:id
router.patch("/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user.notes.includes(req.params.id)) {
            return res.status(400).json({ message: "No note found" })
        }
        const note = await Note.findById(req.params.id)
        await Note.findByIdAndUpdate(req.params.id, {
            completed: !note.completed,
        })
        res.status(200).json({ message: "Note updated" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

module.exports = router
