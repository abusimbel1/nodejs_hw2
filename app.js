const express = require("express")
const config = require("config")
const mongoose = require("mongoose")
var cors = require("cors")

const app = express()

app.use(express.json({ extended: true }))
app.use(cors())

app.use("/api/auth", require("./routes/authRoutes"))
app.use("/api/notes", require("./routes/noteRoutes"))
app.use("/api/users", require("./routes/userRoutes"))

const PORT = config.get("serverPort") || 3333

async function start() {
    try {
        await mongoose.connect(config.get("mongoUri"), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        })
        app.listen(process.env.PORT || PORT, () =>
            console.log(`app has been started on port ${PORT}...`)
        )
    } catch (e) {
        console.log("Server Error", e.message)
        process.exit(1)
    }
}

start()
