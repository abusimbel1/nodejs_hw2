import React from "react"
import { BrowserRouter } from "react-router-dom"
import { useRoutes } from "./routes"
import { AuthContext } from "./context/AuthContext"
import { useAuth } from "./hooks/auth.hook"
import { NavbarComp } from "./components/Navbar"
import { AlertState } from "./context/Alert/AlertState"
import "bootstrap/dist/css/bootstrap.min.css"
import { APIState } from "./context/API/APIState"

function App() {
    const { token, login, logout } = useAuth()
    const isAuthentificated = !!token
    const routes = useRoutes(isAuthentificated)
    console.log(token)
    return (
        <AuthContext.Provider
            value={{ token, login, logout, isAuthentificated }}
        >
            <APIState>
                <AlertState>
                    <BrowserRouter>
                        {isAuthentificated && <NavbarComp />}
                        <div className="container">{routes}</div>
                    </BrowserRouter>
                </AlertState>
            </APIState>
        </AuthContext.Provider>
    )
}

export default App
