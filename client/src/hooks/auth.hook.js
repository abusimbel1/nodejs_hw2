import { useState, useCallback, useEffect } from "react"

const storageName = "userToken"

export const useAuth = () => {
    const [token, setToken] = useState(null)
    const login = useCallback((jwt_token) => {
        setToken(jwt_token)

        localStorage.setItem(storageName, JSON.stringify({ jwt_token }))
    }, [])

    const logout = useCallback(() => {
        setToken(null)
        localStorage.removeItem(storageName)
    }, [])

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName))
        if (data && data.jwt_token) {
            login(data.jwt_token)
        }
    }, [login])

    return { login, logout, token }
}
