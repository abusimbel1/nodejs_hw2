import React, { useReducer } from "react"
import { userContext } from "./userContext"
import { userReducer } from "./userReducer"
import * as axios from "axios"
import { SHOW_USER, DELETE_USER, USER_SHOW_LOADER } from "../types"

const url = "http://localhost:3333/api"

export const UserState = ({ children }) => {
    const initialState = {
        user: {},
        loading: false,
    }

    const [state, dispath] = useReducer(userReducer, initialState)
    const showLoader = () => dispath({ type: USER_SHOW_LOADER })

    const showUser = async (token) => {
        console.log("asdasdas")
        // showLoader()
        const res = await axios.get(`${url}/me`, {
            headers: { Authorization: `Bearer ${token}` },
        })
        const payload = {
            ...res.data,
        }
        console.log(payload)
        dispath({
            type: SHOW_USER,
            payload,
        })
    }

    return (
        <userContext.Provider
            value={{
                showLoader,
                showUser,
                user: state.user,
                loading: state.loading,
            }}
        >
            {children}
        </userContext.Provider>
    )
}
