import { SHOW_USER, DELETE_USER, USER_SHOW_LOADER } from "../types"

const handlers = {
    [USER_SHOW_LOADER]: (state) => ({ ...state, loading: true }),
    [SHOW_USER]: (state, { payload }) => ({
        ...state,
        user: payload,
        loading: false,
    }),
    DEFAULT: (state) => state,
}

export const apiReducer = (state, action) => {
    const handle = handlers[action.type] || handlers.DEFAULT
    return handle(state, action)
}
