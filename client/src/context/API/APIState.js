import React, { useReducer } from "react"
import { APIContext } from "./apiContext"
import { apiReducer } from "./apiReducer"
import * as axios from "axios"
import {
    ADD_NOTE,
    FETCH_NOTES,
    ONE_NOTE,
    REMOVE_NOTE,
    SHOW_LOADER,
} from "../types"

const url = "http://localhost:3333/api"

export const APIState = ({ children }) => {
    const initialState = {
        notes: [],
        loading: false,
    }

    const [state, dispath] = useReducer(apiReducer, initialState)
    const showLoader = () => dispath({ type: SHOW_LOADER })
    const fetchNotes = async (token) => {
        showLoader()
        const res = await axios.get(`${url}/notes`, {
            headers: { Authorization: `Bearer ${token}` },
        })

        const payload = res.data.notes.map((_, key) => {
            return {
                ...res.data.notes[key],
            }
        })
        console.log(payload)

        dispath({
            type: FETCH_NOTES,
            payload,
        })

        console.log("res", res.data)
    }
    const oneNote = async (id, token) => {
        showLoader()
        const res = await axios.get(`${url}/notes/${id}`, {
            headers: { Authorization: `Bearer ${token}` },
        })

        const payload = {
            ...res.data.note,
        }

        dispath({
            type: ONE_NOTE,
            payload,
        })

        console.log("res", res.data)
    }

    const addNote = async (text, token) => {
        const note = {
            text,
        }
        try {
            const res = axios.post(`${url}/notes`, note, {
                headers: { Authorization: `Bearer ${token}` },
            })
            const payload = {
                ...note,
                id: res.data.name,
            }
            dispath({
                type: ADD_NOTE,
                payload,
            })
            console.log("addNote ", res.data)
        } catch (e) {
            throw new Error(e.message)
        }
    }

    const removeNote = async (id, token) => {
        await axios.delete(`${url}/notes/${id}`, {
            headers: { Authorization: `Bearer ${token}` },
        })

        dispath({
            type: REMOVE_NOTE,
            payload: id,
        })
    }

    return (
        <APIContext.Provider
            value={{
                showLoader,
                addNote,
                removeNote,
                fetchNotes,
                oneNote,
                loading: state.loading,
                notes: state.notes,
            }}
        >
            {children}
        </APIContext.Provider>
    )
}
