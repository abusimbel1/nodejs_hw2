import React from "react"
import { Switch, Route, Redirect } from "react-router-dom"
import { AuthPage } from "./pages/AuthPage"
import { CreateNotePage } from "./pages/CreateNotePage"
import { NotePage } from "./pages/NotePage"
import { NotesPage } from "./pages/NotesPage"
import { UserPage } from "./pages/UserPage"

export const useRoutes = (isAuthenticated) => {
    if (isAuthenticated) {
        return (
            <Switch>
                <Route path="/notes" exact>
                    <NotesPage />
                </Route>
                <Route path="/notes/:id" component={NotePage}></Route>
                <Route path="/create" exact>
                    <CreateNotePage />
                </Route>
                <Route path="/me" exact>
                    <UserPage />
                </Route>
                <Redirect to="/me" />
            </Switch>
        )
    }

    return (
        <Switch>
            <Route path="/" exact>
                <AuthPage />
            </Route>
            <Redirect to="/" />
        </Switch>
    )
}
