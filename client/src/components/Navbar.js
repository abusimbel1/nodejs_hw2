import React, { useContext } from "react"
import { Navbar, Nav, Button } from "react-bootstrap"
import { NavLink, useHistory } from "react-router-dom"
import { AuthContext } from "../context/AuthContext"

export const NavbarComp = () => {
    const history = useHistory()
    const auth = useContext(AuthContext)

    const logoutHandler = (event) => {
        event.preventDefault()
        auth.logout()
        history.push("/")
    }

    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="#">Node_Hw_2</Navbar.Brand>
            <Nav className="mr-auto">
                <NavLink to="/me">Profile</NavLink>
                <NavLink to="/notes">Notes</NavLink>
                <NavLink to="/create">Create</NavLink>
            </Nav>
            <Button variant="outline-info" onClick={logoutHandler}>
                Log out
            </Button>
        </Navbar>
    )
}
