import React from "react"
import { ListGroup } from "react-bootstrap"
import { NavLink } from "react-router-dom"

export const Notes = ({ notes }) => {
    return (
        <ListGroup>
            {notes.map((note) => (
                <ListGroup.Item className="note" key={note._id}>
                    <NavLink to={`/notes/${note._id}`}>{note.text}</NavLink>
                </ListGroup.Item>
            ))}
        </ListGroup>
    )
}
