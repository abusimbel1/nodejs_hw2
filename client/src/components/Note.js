import React from "react"
import { Card } from "react-bootstrap"

export const Note = ({ note }) => {
    console.log(note)
    return (
        <Card
            style={{
                width: "18rem",
                // display: "flex",
                // justifyContent: "center",
            }}
        >
            <Card.Body>
                <Card.Title>This is a note</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                    Completed {note.completed ? "true" : "false"}
                </Card.Subtitle>
                <Card.Text>
                    <strong>{note.text}</strong>
                    <br />
                    {note.createdDate}
                </Card.Text>
            </Card.Body>
        </Card>
    )
}
