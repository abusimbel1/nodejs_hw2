import React, { useContext } from "react"
import { Alert } from "react-bootstrap"
import { AlertContext } from "../context/Alert/alertContext"

export const AlertComp = () => {
    const { alert, hide } = useContext(AlertContext)
    if (!alert.visible) {
        return null
    }
    return (
        <Alert
            variant={alert.type || "danger"}
            onClose={() => hide()}
            dismissible
        >
            {alert.text}
        </Alert>
    )
}
