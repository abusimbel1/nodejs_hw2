import React, { useContext, useEffect } from "react"
import { Loader } from "../components/Loader"
import { Notes } from "../components/Notes"
import { APIContext } from "../context/API/apiContext"
import { useAuth } from "../hooks/auth.hook"

export const NotesPage = () => {
    const { token } = useAuth()
    const { loading, notes, fetchNotes } = useContext(APIContext)

    useEffect(() => {
        fetchNotes(token)
        // eslint-disable-next-line
    }, [])
    console.log(notes)
    return <>{loading ? <Loader /> : <Notes notes={notes} />}</>
}
