import React, { useState, useEffect, useContext } from "react"
import { Button, Form } from "react-bootstrap"
import { AuthContext } from "../context/AuthContext"
import { useHttp } from "../hooks/http.hook"
import { AlertComp } from "../components/Alert"
import { AlertContext } from "../context/Alert/alertContext"

export const AuthPage = () => {
    const auth = useContext(AuthContext)
    const alert = useContext(AlertContext)
    const [errorText, setErrorText] = useState("")
    const { loading, request, error, clearError } = useHttp()
    const [form, setForm] = useState({
        email: "",
        password: "",
    })

    useEffect(() => {
        if (errorText) {
            alert.show(errorText)
        }
        setErrorText(error)
        clearError()
    }, [error, clearError, alert, errorText])

    const changeHandler = (event) => {
        setForm({ ...form, [event.target.name]: event.target.value })
    }

    const registerHandler = async () => {
        try {
            await request("/api/auth/register", "POST", {
                ...form,
            })
        } catch (e) {}
    }
    const loginHandler = async () => {
        try {
            const data = await request("/api/auth/login", "POST", {
                ...form,
            })
            auth.login(data.jwt_token)
        } catch (e) {}
    }

    return (
        <div className="authForm">
            <AlertComp />
            <Form>
                <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        controlid="formBasicUsernamel"
                        type="username"
                        placeholder="Enter username"
                        // id="email"
                        name="username"
                        onChange={changeHandler}
                    />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        controlid="formBasicEmail"
                        type="password"
                        placeholder="Password"
                        // id="password"
                        name="password"
                        onChange={changeHandler}
                    />
                </Form.Group>
                <p>{errorText}</p>
                <Button
                    variant="primary"
                    style={{ marginRight: 15 }}
                    onClick={loginHandler}
                    disabled={loading}
                >
                    Log in
                </Button>
                <Button
                    variant="secondary"
                    onClick={registerHandler}
                    disabled={loading}
                >
                    Sign up
                </Button>
            </Form>
        </div>
    )
}
