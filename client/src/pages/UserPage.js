import React, { useContext, useEffect } from "react"
import { Loader } from "../components/Loader"
import { useAuth } from "../hooks/auth.hook"
import { userContext } from "../context/User/userContext"

export const UserPage = () => {
    const { loading, user, showUser } = useContext(userContext)

    const { token } = useAuth()
    useEffect(() => {
        showUser(token)
        // eslint-disable-next-line
    }, [])
    return <>{loading ? <Loader /> : user.username}</>
}
