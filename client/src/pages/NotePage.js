import React, { useContext, useEffect } from "react"
import { Loader } from "../components/Loader"
import { Note } from "../components/Note"
import { useAuth } from "../hooks/auth.hook"
import { APIContext } from "../context/API/apiContext"

export const NotePage = ({ match }) => {
    const { loading, notes, oneNote } = useContext(APIContext)
    const { token } = useAuth()
    useEffect(() => {
        oneNote(match.params.id, token)
        // eslint-disable-next-line
    }, [])
    return <div>{loading ? <Loader /> : <Note note={notes} />}</div>
}
