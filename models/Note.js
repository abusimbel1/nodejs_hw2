const { Schema, model, Types } = require("mongoose")

const schema = new Schema({
    userId: {
        required: true,
        type: String,
        ref: "User",
    },
    completed: {
        type: Boolean,
        default: false,
    },
    text: {
        required: true,
        type: String,
    },
    createdDate: {
        type: Date,
        default: Date.now,
    },
})

module.exports = model("Note", schema)
