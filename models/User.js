const { Schema, model, Types } = require("mongoose")

const schema = new Schema({
    username: {
        required: true,
        type: String,
        unique: true,
    },
    password: {
        required: true,
        type: String,
    },
    notes: [
        {
            type: Types.ObjectId,
            ref: "Note",
        },
    ],
    createdDate: {
        type: Date,
        default: Date.now,
    },
})

module.exports = model("User", schema)
